import { FC, CSSProperties } from "react";
import { Stack, Box } from "@mui/system";
import { Grid, Typography } from "@mui/material";


type PostAuthorDetailsProps = {
    authorName: string;
    timestamp: Date;
};

const postAuthorDetailsStackStyle: CSSProperties = {
    display: "flex",
    alignItems: "center",
    padding: "5px",
    width: "auto",
    maxWidth: "200px",
    maxHeight: "50px",
    backgroundColor: "#1e1e1e",
    color: "#969696",
    borderRadius: "5px",
};

const authorAvatarStyle: CSSProperties = {
    minHeight: "20px",
    minWidth: "20px",
    maxHeight: "30px",
    maxWidth: "30px",
    marginRight: "10px",
    borderRadius: "50%",
    background: "#f44336",
};

const authorTypographyStyle: CSSProperties = {
    fontSize: "8px",
};

const PostAuthorDetails: FC<PostAuthorDetailsProps> = ({
    authorName,
    timestamp,
}) => {

    return (
        <Grid sx={postAuthorDetailsStackStyle}>
            <Box sx={authorAvatarStyle}></Box>
            <Stack>
                <Typography
                    sx={{ ...authorTypographyStyle, fontWeight: "bold" }}>
                    {(authorName)}
                </Typography>
                <Typography sx={authorTypographyStyle}>
                    {timestamp.getFullYear()}
                </Typography>
            </Stack>
        </Grid>
    );
};
export default PostAuthorDetails;
