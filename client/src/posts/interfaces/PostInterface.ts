interface PostInterface {
  _id: string;
  author: string;
  selftext: string;
  sentiment: string;
  created: Date;
  img: string;
  category: string;
  title: string;
}

export default PostInterface;
