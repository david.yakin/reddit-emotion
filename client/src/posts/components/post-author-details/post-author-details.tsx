import { FC } from "react";
import { Stack, Box } from "@mui/system";
import { Grid, Typography } from "@mui/material";
import {
  capitalizeFirstLetterOfEachWord,
  getOnlyFirstLetters,
} from "../../../global/utils/algoMethods";

type PostAuthorDetailsProps = {
  author: string;
  created: string;
};

const PostAuthorDetails: FC<PostAuthorDetailsProps> = ({ author, created }) => {
  return (
    <Grid display="flex" width="70%" color="primary.main">
      <Box
        display="flex"
        justifyContent="center"
        alignItems="center"
        height={35}
        width={35}
        borderRadius="50%"
        color="primary.light"
        bgcolor="warning.main"
        p={0}
        mr={1}
      >
        {author && getOnlyFirstLetters(author)?.slice(0, 2)}
      </Box>
      <Stack>
        <Typography fontSize={12} fontWeight="bold">
          {author && capitalizeFirstLetterOfEachWord(author)}
        </Typography>
        <Typography fontSize={12}>{created.toLocaleString()}</Typography>
      </Stack>
    </Grid>
  );
};
export default PostAuthorDetails;
