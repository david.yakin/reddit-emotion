import { Card, Grid } from "@mui/material";
import PostCardHeader from "./post-card-head/PostCardHead";
import PostCardActionBar from "./post-card-action-bar/PostCardActionBar";
import PostCardBody from "./post-card-body/PostCardBody";
import { FC } from "react";
import NavLink from "../../../../../global/router/components/NavLink";

interface CardsProps {
  imageUrl: string;
  title: string;
  selftext: string;
  postId: string;
  category: string;
}

const PostCard: FC<CardsProps> = ({
  imageUrl,
  title,
  selftext,
  postId,
  category,
}) => {
  return (
    <Grid item xs={12} md={6} lg={4} xl={3}>
      <Card>
        <NavLink to={`/post-details/${category}/${postId}`}>
          <PostCardHeader imageUrl={imageUrl} />
          <PostCardBody title={title} selfText={selftext} />
        </NavLink>
        <PostCardActionBar category={category} postId={postId} />
      </Card>
    </Grid>
  );
};

export default PostCard;
