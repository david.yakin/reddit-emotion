import { CardContent, Typography } from "@mui/material";
import { FC } from "react";
import { truncateText } from "../../../../../../global/utils/algoMethods";

interface PostCardBodyProps {
  title: string;
  selfText: string;
}

const PostCardBody: FC<PostCardBodyProps> = ({ title, selfText }) => {
  const truncatedText = truncateText(selfText, 80);
  const truncatedTitle = truncateText(title, 30);

  return (
    <CardContent>
      <Typography gutterBottom variant="h5" component="div">
        {truncatedTitle}
      </Typography>
      <Typography variant="body2" color="text.secondary">
        {truncatedText}
      </Typography>
    </CardContent>
  );
};

export default PostCardBody;
