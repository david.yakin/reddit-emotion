import { Button, CardActions } from "@mui/material";
import { useNavigate } from "react-router-dom";

export default function PostCardActionBar(props: {category: string, postId: string}) {
  const {category, postId} = props
const navigate = useNavigate()

  return (
    <CardActions>
      <Button size="small" onClick={() => navigate(`/post-details/${category}/${postId}`)}>Learn More</Button>
    </CardActions>
  );
}
