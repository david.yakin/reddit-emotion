import { CardMedia } from "@mui/material";
import { FC } from "react";

interface PostCardHeaderProps {
  imageUrl: string;
}
const PostCardHeader: FC<PostCardHeaderProps> = ({ imageUrl }) => {
  if (!imageUrl?.includes(".jpeg"))
    imageUrl = "/images/reddit-homePage-pic.webp";

  return (
    <CardMedia component="img" image={imageUrl} sx={{ aspectRatio: 16 / 9 }} />
  );
};

export default PostCardHeader;
