import CommentCard from "./comment-card";
import { Comment } from "../../../global/types/types";
import { Box } from "@mui/material";

interface Props {
  comments: Comment[];
}
export default function CommentsCards({ comments }: Props) {
  return (
    <Box>
      {comments.map((comment) => (
        <CommentCard key={comment.created} comment={comment} />
      ))}
    </Box>
  );
}
