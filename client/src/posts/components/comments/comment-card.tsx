import * as React from "react";
import { Box } from "@mui/material";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import { Comment } from "../../../global/types/types";
import Sentiment from "../sentiment/sentiment";
import Divider from "@mui/material/Divider";
import CommentsCards from "./comments-cards";
import ListItemButton from "@mui/material/ListItemButton";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import Collapse from "@mui/material/Collapse";

interface Props {
  comment: Comment;
}


export default function Comments({ comment }: Props) {
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(!open);
  };
  if (!comment?.body?.includes("http"))
    return (
      <List sx={{ bgcolor: "background.paper" }}>
        <ListItemButton onClick={handleClick}>
          <ListItemAvatar>
            <Avatar alt={comment.author} src="/static/images/avatar/1.jpg" />
          </ListItemAvatar>
          <ListItemText primary={comment.author} secondary={comment.body} />
          <Sentiment sentiment={comment.sentiment} size={10} />
          {open ? <ExpandLess /> : <ExpandMore />}
          <Divider variant="inset" component="li" />
        </ListItemButton>

        <Collapse in={open} timeout="auto" unmountOnExit>
          <ListItem>
            <Box px={2}>
              {comment.replies ? (
                <CommentsCards comments={comment.replies} />
              ) : null}
            </Box>
          </ListItem>
        </Collapse>
      </List>
    );
}

