import { TextField, Button, Box } from "@mui/material";
import { FC, useState, Dispatch, SetStateAction, useEffect, memo } from "react";
import useSendRequestToServer from "../../../hooks/useSendRequestToServer";
import Spinner from "../../../../global/components/Spinner";
import { useErrorPage } from "../../../../global/hooks/useErrorPage";
import { toastHandler } from "../../../../global/components/toastify/toastHandler";

interface SelectPostCategoryProps {
  handelSteps: (activeStep: number) => void;
  onSetCategory: Dispatch<
    SetStateAction<{
      category: string;
      subCategory: string;
    }>
  >;
}

const SelectPostCategory: FC<SelectPostCategoryProps> = memo(
  function SelectPostCategory({ handelSteps, onSetCategory }) {
    const {
      data: isExists,
      pending,
      error,
      handelIsCategoryExists,
    } = useSendRequestToServer<boolean>();

    const [chosenCategory, setChosenCategory] = useState("");

    useErrorPage(
      error === `Network Error` ? { status: 503, message: error } : null
    );

    useEffect(() => {
      if (isExists) {
        handelSteps(1);
        onSetCategory((prev) => ({
          ...prev,
          category: chosenCategory,
        }));
      }
    }, [chosenCategory, handelSteps, isExists, onSetCategory]);

    useEffect(() => {
      if (
        !pending &&
        !isExists &&
        error === `Request failed with status code 404`
      ) {
        toastHandler(`Category does not exist, please try another`, `error`);
        setChosenCategory(``);
      }
    }, [error, isExists, pending]);

    if (pending) return <Spinner />;

    return (
      <Box display="flex" flexDirection="column" alignItems="center">
        <TextField
          sx={{ width: "50%", my: "3" }}
          label="Search Category"
          value={chosenCategory}
          onChange={(e) => setChosenCategory(e.target.value)}
        />
        {chosenCategory && (
          <Button
            variant="contained"
            sx={{ mt: 2, fontWeight: "Bold" }}
            onClick={() => handelIsCategoryExists(chosenCategory)}
          >
            Select
          </Button>
        )}
      </Box>
    );
  }
);

export default SelectPostCategory;
