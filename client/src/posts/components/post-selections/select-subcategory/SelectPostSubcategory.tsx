import React, { Dispatch, SetStateAction, useState } from "react";
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Button,
  Box,
  SelectChangeEvent,
} from "@mui/material";

interface SubcategoryProps {
  handelSteps: (activeStep: number) => void;
  onSetSubCategory: Dispatch<
    SetStateAction<{
      category: string;
      subCategory: string;
    }>
  >;
}

const SelectPostSubcategory: React.FC<SubcategoryProps> = ({
  handelSteps,
  onSetSubCategory,
}) => {
  const [chosenSubCategory, setChosenSubCategory] = useState("new");

  const handleSubCategoryChange = (event: SelectChangeEvent<string>) => {
    setChosenSubCategory(event.target.value);
  };

  return (
    <Box display="flex" flexDirection="column" alignItems="center">
      <FormControl sx={{ width: "40%", my: "3" }}>
        <InputLabel id="post-category-label">Select Subcategory</InputLabel>
        <Select
          labelId="post-category-label"
          id="post-category-select"
          value={chosenSubCategory}
          onChange={handleSubCategoryChange}
        >
          <MenuItem value="new">New</MenuItem>
          <MenuItem value="hot">Hot</MenuItem>
          <MenuItem value="rising">Rising</MenuItem>
          <MenuItem value="top">Top</MenuItem>
        </Select>
      </FormControl>
      <Button
        variant="contained"
        sx={{ mt: 2, fontWeight: "Bold" }}
        onClick={() => {
          handelSteps(2);
          onSetSubCategory((prev) => ({
            ...prev,
            subCategory: chosenSubCategory,
          }));
        }}
      >
        Select
      </Button>
    </Box>
  );
};

export default SelectPostSubcategory;
