import { FC } from "react";

type GreetProps = { name?: string };

const Greet: FC<GreetProps> = ({ name = "Shula Zaken" }) => {
  return <div>{name}</div>;
};

export default Greet;
