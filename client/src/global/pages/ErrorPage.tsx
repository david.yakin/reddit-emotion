import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import { useAtom } from "jotai";
import { errorPagePropsAtom } from "../../states/atoms";
import { theme } from "../styles/themes/themeModels";
import NavLink from "../router/components/NavLink";
import ROUTES from "../router/routes-model";

const ErrorPage = () => {
  const [props] = useAtom(errorPagePropsAtom);
  const { status, message } = props;
  return (
    <>
      <Grid sx={{ display: `flex`, alignItems: `center`, flexDirection: `column`, margin: `10vh 2vh 0 2vh` }}>
        <Typography variant="h1">{status}</Typography>
        <Typography variant="h2" sx={{ color: theme.palette.warning.main }}>
          ERROR
        </Typography>
        <Typography variant="h4" sx={{ margin: `20px 0 50px 0` }}>
          {message ? message : `Oops, something went wrong`}
        </Typography>
        <NavLink to={ROUTES.HOME} color={theme.palette.info.main} sx={{ marginBottom: `24px`, fontSize: `200%` }}>
          Return to Home page...
        </NavLink>{" "}
      </Grid>
    </>
  );
};

export default ErrorPage;
