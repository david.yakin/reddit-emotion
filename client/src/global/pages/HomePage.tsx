// YourPage.tsx
import { Grid, Typography, CardMedia } from "@mui/material";
import PageHeader from "../components/PageHeader"; // Adjust the import path based on your project structure
import NavLink from "../router/components/NavLink";
import { theme } from "../styles/themes/themeModels";

const HomePage: React.FC = () => {
  return (
    <>
      <PageHeader
        title="reddit Emotions Site"
        subtitle="reddit is home to thousands of communities, endless conversation, and authentic human connection."
      />
      <Grid container justifyContent="center" alignItems="center">
        <Grid item>
          <Typography textAlign="center" variant="h6" component="h2" color="#454547">
            <NavLink to="/select-post" color={theme.palette.info.main}>
              For More Details...
            </NavLink>
          </Typography>
          <CardMedia
            sx={{
              objectFit: "cover",
              maxWidth: "100vh",
              maxHeight: "100vh",
              aspectRatio: 16 / 9,
              p: 2,
            }}
            component="img"
            alt="Reddit Emotions Site"
            image="../../../images/reddit-homePage-pic.webp"
          />
        </Grid>
      </Grid>
    </>
  );
};

export default HomePage;
