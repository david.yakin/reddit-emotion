import { render } from "@testing-library/react";
import LogoIcon from "./LogoIcon";

describe("LogoIcon testing", () => {
  it("renders the logo image", () => {
    const { getByAltText } = render(<LogoIcon />);
    const logo = getByAltText("Logo");
    expect(logo).toBeTruthy();
  });

  it("renders the IconButton", () => {
    const { getByRole } = render(<LogoIcon />);
    expect(getByRole("button")).toBeTruthy();
  });
  it("renders alt text", () => {
    const { getByAltText } = render(<LogoIcon />);
    expect(getByAltText("Logo")).toBeTruthy();
  });
});
