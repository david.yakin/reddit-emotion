import { Typography } from "@mui/material";
import { Box } from "@mui/system";
import { CSSProperties, FC } from "react";

type LogoProps = {
  sx?: CSSProperties;
};
const Logo: FC<LogoProps> = ({ sx }) => {
  return (
    <Box
      sx={{ display: { xs: "none", md: "flex" }, alignItems: "center", ...sx }}
    >
      <Typography>Reddit Emotion</Typography>
    </Box>
  );
};

export default Logo;
