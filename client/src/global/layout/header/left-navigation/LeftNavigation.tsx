import { Grid } from "@mui/material";
import Logo from "../../logo/Logo";
import LogoIcon from "../../logo/LogoIcon";
import NavLink from "../../../router/components/NavLink";

const LeftNavigation = () => {
  return (
    <Grid item>
      <NavLink
        to="/"
        sx={{
          display: "flex",
          flexDirection: "row-reverse",
          color: "white",
        }}
      >
        <Logo />
        <LogoIcon />
      </NavLink>
    </Grid>
  );
};

export default LeftNavigation;
