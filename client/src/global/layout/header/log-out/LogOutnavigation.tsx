import { Grid, Stack, Button, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import UserAvatar from ".././avatar/UserAvatar";
import ROUTES from "../../../router/routes-model";
import { useAuth } from "../../../../auth/AuthProvider";

const LogOutNavigation = () => {
  const { setToken } = useAuth();
  const navigate = useNavigate();

  return (
    <Grid
      item
      sx={{
        cursor: "pointer",
      }}
    >
      <Stack direction="row" gap={2}>
        <Button
          sx={{ color: "white" }}
          onClick={() => {
            setToken(null);
            localStorage.removeItem("token");
            navigate(ROUTES.HOME);
          }}
        >
          <Typography>Log Out</Typography>
        </Button>
        <UserAvatar />
      </Stack>
    </Grid>
  );
};

export default LogOutNavigation;
