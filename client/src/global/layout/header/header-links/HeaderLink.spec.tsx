import { render } from "@testing-library/react";
import HeaderLink from "./HeaderLink";

describe("HeaderLink testing", () => {
  it("renders children", () => {
    const { getByText } = render(<HeaderLink>Test</HeaderLink>);
    expect(getByText("Test")).toBeTruthy();
  });

  // it("applies sx prop to Typography", () => {
  //   const sx = { color: "red" };
  //   const { getByText } = render(<HeaderLink sx={sx}>Test</HeaderLink>);
  //   expect(getByText("Test")).toHaveStyle(`color: ${sx.color}`);
  // });
});
