import { render, screen } from "@testing-library/react";
import CenterNavigation from "./CenterNavigation";

describe("CenterNavigation", () => {
  test("renders header link", () => {
    render(<CenterNavigation />);
    const link = screen.getByText(/my history/i);
    expect(link).toBeTruthy();
  });


});
