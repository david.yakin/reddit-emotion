import HeaderLink from "../header-links/HeaderLink";

const CenterNavigation = () => {
  return <HeaderLink sx={{ display: "flex" }}>my history</HeaderLink>;
};

export default CenterNavigation;
