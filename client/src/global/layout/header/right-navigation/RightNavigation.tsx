import { Divider, Grid, Stack } from "@mui/material";
import NavLink from "../../../router/components/NavLink";

const RightNavigation = () => {
  return (
    <Grid
      item
      sx={{
        cursor: "pointer",
      }}
    >
      <Stack
        direction="row"
        gap={2}
        divider={<Divider orientation="vertical" flexItem />}
      >
        <NavLink to="/login" color="white">
          Login
        </NavLink>
        <NavLink to="/signup" color="white">
          SingUp
        </NavLink>
      </Stack>
    </Grid>
  );
};

export default RightNavigation;
