import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import Footer from "./Footer";

describe("Footer testing", () => {
  test("renders the footer links", () => {
    render(
      <BrowserRouter>
        <Footer />
      </BrowserRouter>
    );

    const phoneLink = screen.getByText("+972-5005858");
    const createdByLink = screen.getByText("Created By: Reddit Emotion Itt");
    const emailLink = screen.getByText("reddit@gmail.com");

    expect(phoneLink).toBeTruthy();
    expect(createdByLink).toBeTruthy();
    expect(emailLink).toBeTruthy();
  });
});
