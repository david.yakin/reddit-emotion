import FooterLink from "./FooterLink";
import { render, screen } from "@testing-library/react";

describe("FooterLink testing", () => {
  it("renders children", () => {
    const children = "Test";
    render(<FooterLink>{children}</FooterLink>);
    expect(screen.getByText(children)).toBeTruthy();
  });
  it("renders sx", () => {
    const sx = { color: "red" };
    render(<FooterLink sx={sx}>Test</FooterLink>);
    expect(screen.getByText("Test")).toBeTruthy();
  });
});
