import { createTheme } from "@mui/material/styles";
import { grey, blue, deepOrange, green, yellow } from "@mui/material/colors";


declare module "@mui/material/styles" {
  interface PaletteColor {
    lighter?: string;
    darker?: string;
  }

  interface SimplePaletteColorOptions {
    lighter?: string;
    darker?: string;
  }
}

export const theme = createTheme({
  typography: {
    fontFamily: "",
  },
  palette: {
    primary: {
      lighter: grey[200],
      light: grey[400],
      main: grey[600],
      dark: grey[800],
      darker: grey[900],
    },
    secondary: {
      lighter: yellow[200],
      light: yellow[400],
      main: yellow[600],
      dark: yellow[800],
      darker: yellow[900],
    },
    info: {
      lighter: blue[100],
      light: blue[300],
      main: blue[500],
      dark: blue[700],
      darker: blue[900],
    },
    warning: {
      lighter: deepOrange["A100"],
      light: deepOrange["A200"],
      main: "#ff4400",
      dark: "#bb3200",
      darker: "#8a2500",
    },
    success: {
      lighter: green[300],
      light: green[400],
      main: green[500],
      dark: green[600],
      darker: green[700],
    },

    mode: "dark",
  },
});
