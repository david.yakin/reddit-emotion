import { AxiosError } from "axios";

export const getErrorMessage = <T>(error: T) => {
  if (error instanceof AxiosError || error instanceof Error) 
    return error.message;
  return "Oops... something went wrong";
};

export const sortArrayOfObject = <T>(array: T[], key: keyof T) => {
  return [
    ...array.sort((a, b) => {
      if (a[key] < b[key]) return -1;
      if (a[key] > b[key]) return 1;
      return 0;
    }),
  ];
};

export const sortReverseArrayOfObject = <T>(array: T[], key: keyof T) => {
  return [
    ...array.sort((a, b) => {
      if (a[key] > b[key]) return -1;
      if (a[key] < b[key]) return 1;
      return 0;
    }),
  ];
};

export const filterArrayOfObjectsByTerm = <T>(
  term: string,
  array: T[],
  key: keyof T
): T[] => {
  const searchTerm = term.trim().toLowerCase();
  const newArray = [...array];
  const arrayFiltered = newArray.filter((object) => {
    const propertyValue = object[key] as unknown as string;
    if (typeof propertyValue !== "string") return false;
    return propertyValue.toLowerCase().includes(searchTerm);
  });
  return arrayFiltered;
};

export const makeFirstLetterCapital = (string: string) => {
  const term = string.toLowerCase().trim();
  return term.charAt(0).toUpperCase() + term.slice(1);
};

export const capitalizeFirstLetterOfEachWord = (string: string) => {
  const regex = /\b\w/g;
  return string.toLowerCase().replace(regex, (letter) => {
    return letter.toUpperCase();
  });
};

export const getOnlyFirstLetters = (string: string) => {
  const regex = /\b(\w)/g;
  return string.toUpperCase().match(regex)?.join("");
};

export const truncateText = (text: string, maxLength: number): string => {
  if(!text) return '';
  if (text.length > maxLength) {
    return text.slice(0, maxLength) + "...";
  }
  return text;
};

export const parseDateString = (date: Date): string => {
  const options: Intl.DateTimeFormatOptions = {
    month: "long",
    day: "numeric",
    year: "numeric",
  };

  return date.toLocaleDateString("en-US", options);
};

export const DoesTokenExsit = (token: string | null) => {
  if (!token) return false;
  return true;
};

export const parseJwt = (token: string) => {
  const base64Url = token.split(".")[1];
  const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  const jsonPayload = decodeURIComponent(
    atob(base64)
      .split("")
      .map(function (c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join("")
  );

  return JSON.parse(jsonPayload);
};
