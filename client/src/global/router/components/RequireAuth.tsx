import React, { ReactNode  } from 'react';
import { Navigate, useLocation } from 'react-router-dom';
import { toastHandler } from '../../components/toastify/toastHandler';
import { useAuth } from '../../../auth/AuthProvider';

interface RequireAuthProps {
    children: ReactNode; 
  }

  const RequireAuth: React.FC<RequireAuthProps> = ({ children }) => {
      const {token} = useAuth()
      const location = useLocation();

  if (!token) {
    toastHandler('You must be logged in to view this page.', 'warn');
    return <Navigate to="/login" state={{ from: location }} replace />;
  }

  return <>{children}</>;
};
export default RequireAuth
