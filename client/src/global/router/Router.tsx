import { Route, Routes } from "react-router-dom";
import ROUTES from "./routes-model";
import HomePage from "../pages/HomePage";
import LoginPage from "../../users/pages/login-page/LoginPage";
import SignupPage from "../../users/pages/sign-up-page/SignupPage";
import SelectPostsPage from "../../posts/pages/select-posts-page/SelectPostsPage";
import PostDetailsPage from "../../posts/pages/post-details-page/post-details-page";
import Error404Page from "../pages/Error404Page";
import ProfilePage from "../../users/pages/profile/ProfilePage";
import ErrorPage from "../pages/ErrorPage";
import UserHistoryPage from "../../users/pages/userHistoryPage/UserHistoryPage";
import UserPage from "../../users/pages/profile/UserPage";
import ResetPassword from "../../users/pages/profile/ResetPassword";
import EmailVerification from "../../users/pages/profile/EmailVerification";
import RequireAuth from "./components/RequireAuth";

const Router: React.FC = () => {

  return (
    <Routes>
      <Route index element={<HomePage />} />
      <Route path={ROUTES.LOGIN} element={<LoginPage />} />
      <Route path={ROUTES.SIGNUP} element={<SignupPage />} />
      <Route path={ROUTES.SELECT_POST} element={<SelectPostsPage />} />
      <Route path={ROUTES.POST_DETAILS} element={<PostDetailsPage />} />
      <Route path={ROUTES.ERROR_PAGE} element={<ErrorPage />} />
      <Route
        path={ROUTES.POSTS_HISTORY}
        element={
          <RequireAuth>
            <UserHistoryPage />
          </RequireAuth>
        }
      />

      <Route
        path={ROUTES.PROFILE}
        element={
          <RequireAuth>
            <ProfilePage />
          </RequireAuth>
        }
      />
      <Route path={ROUTES.USER_PAGE} element={<UserPage />} />
      <Route path={ROUTES.EMAIL_VERIFY} element={<EmailVerification />} />
      <Route path={ROUTES.RESET_PASSWORD} element={<ResetPassword />} />
      <Route path={ROUTES.ERROR_404_PAGE} element={<Error404Page />} />
    </Routes>
  );
};

export default Router;
