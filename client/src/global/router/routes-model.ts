const ROUTES = {
  HOME: "/",
  LOGIN: "/login",
  SIGNUP: "/signup",
  SELECT_POST: "/select-post",
  POST_DETAILS: "/post-details/:category/:postId",
  POSTS_HISTORY: "/history",
  ERROR_PAGE: "/error",
  ERROR_404_PAGE: "*",
  PROFILE: "/profile",
  USER_PAGE: "/user-page",
  EMAIL_VERIFY: "/email-verify",
  RESET_PASSWORD: "/reset-password",
  HISTORY_PAGE: "/unauthorized-page",
};

export default ROUTES;
