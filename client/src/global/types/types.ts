import PostInterface from "../../posts/interfaces/PostInterface";

export interface Categories {
  data: {
    title: string;
    name: string;
    display_name: string;
  };
}

export enum SentimentEnum {
  Positive = "positive",
  Negative = "negative",
}

export interface Post {
  created: string;
  author: string;
  title: string;
  id: string;
  img: string;
  selftext: string;
  sentiment: string;
}

export interface UserFormInput {
  name?: {
    first: string;
    last: string;
  };
  email: string;
  password: string;
  userId: string;
}

export interface Comment {
  sentiment: string;
  body: string;
  author: string;
  created: string;
  replies?: Comment[];
}

export interface PostWithComments {
  comments: Comment[];
  post: PostInterface;
}

export interface ErrorPageProps {
  status: number;
  message?: string;
}
