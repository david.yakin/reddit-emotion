import { ChangeEvent, Dispatch, SetStateAction, useEffect } from "react";
import { filterArrayOfObjectsByTerm } from "../utils/algoMethods";
import { Box, Grid, IconButton, InputBase } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";

type FilterableInputProps<T> = {
  array: T[];
  keyOfObject: keyof T;
  onChange: Dispatch<SetStateAction<T[] | null>>;
};

const FilterableInput = <P extends object>({
  array,
  keyOfObject,
  onChange,
}: FilterableInputProps<P>) => {
  useEffect(() => {
    onChange(array);
  }, []);

  const handleSearch = (e: ChangeEvent<HTMLInputElement>) => {
    const term = e.target.value.trim();
    const filteredArray = filterArrayOfObjectsByTerm(term, array, keyOfObject);
    onChange(() => filteredArray);
  };

  return (
    <Grid container justifyContent="center">
      <Grid item>
        <Box p={2} minWidth="350px" component="form">
          <InputBase
            sx={{ flex: 1 }}
            placeholder="Search Post"
            inputProps={{ "aria-label": "search post" }}
            onChange={handleSearch}
          />
          <IconButton type="button" aria-label="search">
            <SearchIcon />
          </IconButton>
        </Box>
      </Grid>
    </Grid>
  );
};

export default FilterableInput;
