import { Box, Typography } from "@mui/material"
import NavLink from "../router/components/NavLink"
import { theme } from "../styles/themes/themeModels"

interface PropsLine {
    text: string;
    to: string;
}


const TextLink = (props: PropsLine) => {
    return (
        <Box>
            <Typography sx={{margin: 2}} variant="h6">
                {props.text}?{" "} 
                <NavLink to={props.to} color={theme.palette.info.light}>
                  Click here...
                </NavLink>
            </Typography>
        </Box>
    )
}
export default TextLink;
