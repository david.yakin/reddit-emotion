import { toast } from "react-toastify";
import { toastStyles } from "./toastStyles";

type ToastType = `warn` | `error` | `info` | `success` | `default`;

export const toastHandler = (message: string, type: ToastType = `default`) => {
  const { errorStyle, infoStyle, successStyle, warnStyle, defaultStyle } = toastStyles;
  switch (type) {
    case `error`:
      toast.error(message, errorStyle);
      break;
    case `warn`:
      toast.warn(message, warnStyle);
      break;
    case `success`:
      toast.success(message, successStyle);
      break;
    case `info`:
      toast.info(message, infoStyle);
      break;
    default:
      toast(message, defaultStyle);
      break;
  }
};
