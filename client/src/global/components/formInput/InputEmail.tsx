import { Box, TextField, Typography } from "@mui/material";
import { FromProps } from "./propsType";
import { theme } from "../../styles/themes/themeModels";
import { userEmailRegex } from "../../utils/regexs";

const InputEmail =  (props: FromProps) => {
    return (
        <Box>
            <TextField
                margin="normal"
                fullWidth
                variant="outlined"
                label="email"
                type="email"
                {...props.register("email", { required: "email is required",
                pattern: {
                value: userEmailRegex,
                message: "Invalid email format."}})}
                onBlur={() => props.trigger("email")}
            />
            {props.formState.errors.email && <Typography color={theme.palette.error.light}>{props.formState.errors.email.message}</Typography>}
        </Box>
    )
}

export default InputEmail;
