import { Box, TextField, Typography } from "@mui/material"
import { FromProps } from "./propsType"
import { theme } from "../../styles/themes/themeModels"

const InputName = (props: FromProps) => {
    return (
        <Box>
            <TextField
            margin="normal"
            fullWidth
            variant="outlined"
            label={`last Name`}
            type="string"
            {...props.register("name.last", { required: "last name is required" ,
            maxLength: { value: 10, message: "last name must be at most 15 characters" },
            minLength: { value: 2, message: "last name must be at least 2 characters" } })}
            onBlur={() => props.trigger("name.last")}
          />
          {props.formState.errors.name?.last && <Typography color={theme.palette.error.light}>{props.formState.errors.name.last.message}</Typography>}
        </Box>
    )
}
export default InputName;