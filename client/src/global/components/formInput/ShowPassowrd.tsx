import { Visibility, VisibilityOff } from "@mui/icons-material";
import { IconButton, InputAdornment } from "@mui/material";

interface StateProps {
    showPassword: boolean;
    setShowPassword: React.Dispatch<React.SetStateAction<boolean>>
}

const ShowPassowrd = (props: StateProps) => {
    const handleClickShowPassword = () => props.setShowPassword(!props.showPassword);
    const handleMouseDownPassword = () => props.setShowPassword(!props.showPassword);

    return (
        <InputAdornment position="end">
            <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
            >
                {props.showPassword ? <Visibility /> : <VisibilityOff />}
            </IconButton>
        </InputAdornment>
    )
}

export default ShowPassowrd;