import { FormControl } from "@mui/material";
import InputEmail from "../../../global/components/formInput/InputEmail";
import InputPassword from "../../../global/components/formInput/InputPassowrd";
import InputLastName from "../../../global/components/formInput/inputLastName";
import InputFirstName from "../../../global/components/formInput/InputFirstName";
import { FromProps } from "../../../global/components/formInput/propsType";

const InputFields = (props: FromProps) => {
    return (
        <FormControl fullWidth>
          <InputFirstName register={props.register} formState={props.formState} trigger={props.trigger} />
          <InputLastName register={props.register} formState={props.formState} trigger={props.trigger} />
          <InputEmail register={props.register} formState={props.formState} trigger={props.trigger} />
          <InputPassword register={props.register} formState={props.formState} trigger={props.trigger} />
         </FormControl>
    )
}
export default InputFields;



