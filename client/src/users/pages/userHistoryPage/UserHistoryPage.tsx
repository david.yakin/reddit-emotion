import { useEffect, useState } from "react";
import UserHistoryHeaderPage from "./components/UserHistoryHeaderPage";
import { UserHistoryPostsTable } from "./components/UserHistoryPostsTable";
import PostsCards from "../../../posts/components/posts-display/posts-cards/PostsCards";
import UserHistoryPageList from "./components/UsrHistoryPageList";
import { Box } from "@mui/system";
import UserHistoryControlBar from "./components/UserHistoryControlBar";
import PostInterface from "../../../posts/interfaces/PostInterface";
import useSendRequestToServer from "../../../posts/hooks/useSendRequestToServer";
import Spinner from "../../../global/components/Spinner";
import { useAtom } from "jotai";
import { tokenAtom } from "../../../states/atoms";
import { useErrorPage } from "../../../global/hooks/useErrorPage";
import { Container } from "@mui/material";
export default function UserHistoryPage() {
  const [token] = useAtom(tokenAtom);

  const [view, setView] = useState<string>("list");
  const [sentimentFilter, setSentimentFilter] = useState<string>("ALL");

  const { data, error, pending, handleGetUserHistory } =
    useSendRequestToServer<PostInterface[]>();

  useEffect(() => {
    handleGetUserHistory(token!);
  }, []);
  useErrorPage(error ? { status: 404, message: error } : null);

  if (pending)
    return (
      <div style={{ height: "400px" }}>
        <Spinner />
      </div>
    );

  if (data) {
    const filterBySentiment = (
      data: PostInterface[],
      sentimentFilter: string
    ) => {
      let ans: PostInterface[];
      sentimentFilter === "ALL"
        ? (ans = data)
        : (ans = data.filter((post) => {
            return post.sentiment === sentimentFilter;
          }));
      return ans;
    };
    const filteredData = filterBySentiment(data, sentimentFilter);
    const RenderData = (view: string) => {
      if (view === "table") {
        return <UserHistoryPostsTable posts={filteredData} />;
      } else if (view === "list") {
        return <UserHistoryPageList posts={filteredData} />;
      } else {
        return <PostsCards cards={filteredData} />;
      }
    };

    return (
      <>
        <Container>
          <UserHistoryHeaderPage />
          <UserHistoryControlBar
            data={filteredData}
            setSentimentFilter={setSentimentFilter}
            setView={setView}
          />
          <Box sx={{ margin: "10px", marginBottom: "15px" }}>
            {RenderData(view)}
          </Box>
        </Container>
      </>
    );
  }
}
