import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import RedditIcon from "@mui/icons-material/Reddit";
import { ListItemAvatar, Avatar } from "@mui/material";
import PostInterface from "../../../../posts/interfaces/PostInterface";
import { Link } from "react-router-dom";
export default function UserHistoryPageList({
  posts,
}: {
  posts: PostInterface[];
}) {
  return (
    <List sx={{ width: "98%", bgcolor: "background.paper", margin: "5px" }}>
      {posts.map((post) => (
        <ListItem key={post._id}>
          <Link
            to={`/post-details/${post.category}/${post._id}`}
            style={{
              textDecoration: "none",
              color: "#dd0000",
              display: "inline-flex",
            }}
          >
            <ListItemAvatar>
              <Avatar>
                <RedditIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary={post.title} secondary={post.author} />
          </Link>
        </ListItem>
      ))}
    </List>
  );
}
