/* eslint-disable @typescript-eslint/no-explicit-any */
import FormControl from "@mui/material/FormControl";
import { Box, InputLabel, NativeSelect } from "@mui/material";

export default function ViewSwitch({
  setView,
}: {
  setView: React.Dispatch<React.SetStateAction<string>>;
}) {
  return (
    <Box sx={{ minWidth: 120, padding: "10px" }}>
      <FormControl fullWidth>
        <InputLabel variant="standard" htmlFor="uncontrolled-native">
          View:
        </InputLabel>
        <NativeSelect
          onChange={(e) => {
            setView(e.target.value);
          }}
          defaultValue={"list"}
          inputProps={{
            name: "age",
            id: "uncontrolled-native",
          }}
        >
          <option value={"table"}>Table</option>
          <option value={"card"}>Cards</option>
          <option value={"list"}>List</option>
        </NativeSelect>
      </FormControl>
    </Box>
  );
}
