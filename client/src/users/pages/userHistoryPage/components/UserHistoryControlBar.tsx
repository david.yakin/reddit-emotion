import SentimentSwitch from "./SentimentSwitch";
import ViewSwitch from "./ViewSwitch";
import { Box, Stack } from "@mui/system";
import React from "react";
import UserHistorySearchLine from "./UserHistorySearchLine";
import PostInterface from "../../../../posts/interfaces/PostInterface";

export default function UserHistoryControlBar({
  setView,
  setSentimentFilter,
  data,
}: {
  setView: React.Dispatch<React.SetStateAction<string>>;
  setSentimentFilter: React.Dispatch<React.SetStateAction<string>>;
  data: PostInterface[];
}) {
  return (
    <>
      <Stack
        sx={{
          justifyContent: "space-evenly",
          flexDirection: "row",
          backgroundColor: "#444444",
          alignItems: "center",
          width: "98%",
          margin: "1%",
          padding: "1%",
        }}
      >
        <ViewSwitch setView={setView} />
        <Box alignItems="center">
          <UserHistorySearchLine data={data} />
        </Box>
        <SentimentSwitch setSentimentFilter={setSentimentFilter} />
      </Stack>
    </>
  );
}
