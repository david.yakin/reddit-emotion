import { FormControl } from "@mui/base";
import { InputLabel, NativeSelect } from "@mui/material";
import { Box } from "@mui/system";

export default function SentimentSwitch({
  setSentimentFilter,
}: {
  setSentimentFilter: React.Dispatch<React.SetStateAction<string>>;
}) {
  return (
    <Box sx={{ minWidth: 120, padding: "10px" }}>
      <FormControl>
        <InputLabel variant="standard" htmlFor="uncontrolled-native">
          Sentiment Filter:
        </InputLabel>
        <NativeSelect
          defaultValue={"All"}
          onChange={(e) => {
            setSentimentFilter(e.target.value);
          }}
          inputProps={{
            name: "age",
            id: "uncontrolled-native",
          }}
        >
          <option value={"ALL"}>All</option>
          <option value={"POSITIVE"}>POSITIVE</option>
          <option value={"NEGATIVE"}>NEGATIVE</option>
          <option value={"NEUTRAL"}>NEUTRAL</option>
        </NativeSelect>
      </FormControl>
    </Box>
  );
}
