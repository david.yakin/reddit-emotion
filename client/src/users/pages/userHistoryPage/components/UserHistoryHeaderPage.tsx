import { Box } from "@mui/material";

export default function UserHistoryHeaderPage() {
  return (
    <>
      <Box sx={{ padding: "25px" }}>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            margin: "0",
            fontSize: "2.125rem",
            color: "white",
            lineHeight: "1.235",
            fontWeight: "400",
            marginTop: "3vh",
            marginBottom: "16px",
          }}
        >
          <h3>My History</h3>
        </Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            margin: "0",
            fontSize: "1.25rem",
            color: "#757575",
            lineHeight: "1.6",
            fontWeight: "500",
          }}
        >
          <p>Here you can find the posts you have viewed</p>
        </Box>
      </Box>
    </>
  );
}
