// import { Box, Card, CardContent, CardHeader, Divider } from '@mui/material';
// import axios from 'axios';
// import { useAtom } from 'jotai';
// import { userFirstNameAtom, userLastNameAtom, userEmailAtom, userIdAtom } from '../../../states/atoms';
// import ProfileFieldEditor from '../profile/ProfileFieldEditor'; 

// export default function Profile() {
//   const [userId] = useAtom(userIdAtom);
//   const [firstName, setFirstName] = useAtom(userFirstNameAtom);
//   const [lastName, setLastName] = useAtom(userLastNameAtom);
//   const [email, setEmail] = useAtom(userEmailAtom);



//   const handleSave = async (field: 'firstName' | 'lastName' | 'email', newValue: string) => {
//     try {
//       const data = await axios.patch(`api/users/${userId}`, { [field]: newValue });
//       if (field === 'firstName') {
//         setFirstName(data.data.name.first);
//         localStorage.setItem("firstName", data.data.name.first);
//       } else if (field === 'lastName') {
//         setLastName(data.data.name.last);
//         localStorage.setItem("lastName", data.data.name.last);
//       } else if (field === 'email') {
//         setEmail(data.data.email);
//         localStorage.setItem("email", data.data.email);
//       }
//     } catch (error) {
//       console.error("Error updating:", error);
//     }
//   };

//   return (
//     <Card sx={{ maxWidth: 450, margin: 2, padding: 1, marginLeft: 50, marginTop: 8, border: "1px solid white" }}>
//       <CardHeader title="Profile" />
//       <Divider />
//       <CardContent>
//         <Box sx={{ display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center", gap: "20px" }}>
//           <ProfileFieldEditor label={firstName||'First Name'} value={firstName || ""} onSave={(newValue) => handleSave('firstName', newValue)} />
//           <ProfileFieldEditor label={lastName || "Last Name"} value={lastName || ""} onSave={(newValue) => handleSave('lastName', newValue)} />
//           <ProfileFieldEditor label= {email || "Email"} value={email || ""} onSave={(newValue) => handleSave('email', newValue)} />
//         </Box>
//       </CardContent>
//     </Card>
//   );
// }
