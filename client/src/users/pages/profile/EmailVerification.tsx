import { useState } from "react";
import {
  Box,
  Card,
  CardContent,
  CardHeader,
  TextField,
  Button,
  useMediaQuery,
} from "@mui/material";
import axios from "axios";

export default function EmailVerification() {
  const [email, setEmail] = useState("");
  const [, setError] = useState("");

  const isSmallScreen = useMediaQuery("(max-width:600px)");

  const handleVerify = async () => {
    try {
      //   await axios.post(`api/verifyEmail/${userId}`, { code: verificationCode });
      await axios.post(`api/verifyEmail${email}`);
      // Handle successful verification
    } catch (error) {
      setError("Verification failed. Please check the verification code.");
    }
  };

  return (
    <Card
      sx={{
        maxWidth: 450,
        margin: isSmallScreen ? "auto" : 2,
        padding: 1,
        marginLeft: isSmallScreen ? "auto" : 50,
        border: "1px solid white",
        marginTop: 10,
      }}
    >
      <CardHeader title="Email Verification" />
      <CardContent>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            gap: "20px",
          }}
        >
          <TextField
            label="Email"
            type="email"
            variant="outlined"
            margin="normal"
            fullWidth
            required
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <Button onClick={handleVerify} variant="contained" color="primary">
            Verify Email
          </Button>
        </Box>
      </CardContent>
    </Card>
  );
}
