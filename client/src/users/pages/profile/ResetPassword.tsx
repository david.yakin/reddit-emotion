import { useState } from "react";
import {
  Box,
  Card,
  CardContent,
  CardHeader,
  Divider,
  TextField,
  Button,
  useMediaQuery,
} from "@mui/material";
import axios from "axios";
import { useAtom } from "jotai";
import { userIdAtom } from "../../../states/atoms";

export default function ResetPassword() {
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [error, setError] = useState("");
  const userId = useAtom(userIdAtom);
  const isSmallScreen = useMediaQuery("(max-width:600px)");

  const handleResetPassword = async () => {
    try {
      if (newPassword !== confirmPassword) {
        setError("Passwords do not match.");
        return;
      }
      await axios.post(`api/resetPassword/${userId}`, { newPassword });
      // Handle successful password reset
    } catch (error) {
      setError("Password reset failed. Please try again later.");
    }
  };

  return (
    <Card
      sx={{
        maxWidth: 450,
        margin: isSmallScreen ? "auto" : 2,
        padding: 1,
        marginLeft: isSmallScreen ? "auto" : 52,
        marginTop: 8,
      }}
    >
      <CardHeader title="Reset Password" />
      <Divider />
      <CardContent>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            gap: "20px",
          }}
        >
          <TextField
            value={newPassword}
            fullWidth
            onChange={(e) => setNewPassword(e.target.value)}
            label="New Password"
            variant="outlined"
            type="password"
          />
          <TextField
            value={confirmPassword}
            fullWidth
            onChange={(e) => setConfirmPassword(e.target.value)}
            label="Confirm Password"
            variant="outlined"
            type="password"
            error={error !== ""}
            helperText={error}
          />
          <Button
            onClick={handleResetPassword}
            variant="contained"
            color="primary"
          >
            Reset Password
          </Button>
        </Box>
      </CardContent>
    </Card>
  );
}
