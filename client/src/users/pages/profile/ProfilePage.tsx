import React, { useState } from 'react';
import { Typography, Modal, Box, Divider } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../../../auth/AuthProvider';
// import { json } from 'stream/consumers';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const ProfilePage: React.FC = () => {
  const {user} = useAuth();
  console.log(user);
  
  const userName = JSON.parse(user?.name as unknown as string || "") || null;
  const [open, setOpen] = useState(true); 
  const navigate = useNavigate();

  const handleClose = () => {
    setOpen(false)
    navigate(-1)
  };

  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style} textAlign="center">
        <Typography id="modal-modal-title" variant="h4" component="h2" gutterBottom>
          Profile
        </Typography>
        <Divider />
        <Typography id="modal-modal-description" sx={{ mt: 2 }} variant="h6">
          First Name: {userName?.first}
        </Typography>
        <Typography sx={{ mt: 2 }} variant="h6">
          Last Name: {userName?.last}
        </Typography>
      </Box>
    </Modal>
  );
};

export default ProfilePage;
