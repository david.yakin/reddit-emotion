import React, { useState } from 'react';
import { TextField, Button } from '@mui/material';

interface ProfileFieldEditorProps {
  label: string;
  value: string;
  onSave: (newValue: string) => Promise<void>;
}

const ProfileFieldEditor: React.FC<ProfileFieldEditorProps> = ({ label, value, onSave }) => {
  const [editMode, setEditMode] = useState(false);
  const [newValue, setNewValue] = useState(value);

  const handleSave = async () => {
    await onSave(newValue);
    setEditMode(false);
  };

  const handleCancel = () => {
    setNewValue(value); 
    setEditMode(false);
  };

  return (
    <>
      <TextField
        value={editMode ? newValue : value}
        onChange={(e) => setNewValue(e.target.value)}
        label={label}
        variant="outlined"
        disabled={!editMode}
      />
      {editMode ? (
        <>
          <Button onClick={handleSave}>Save</Button>
          <Button onClick={handleCancel}>Cancel</Button>
        </>
      ) : (
        <Button onClick={() => setEditMode(true)}>Edit</Button>
      )}
    </>
  );
};

 export default ProfileFieldEditor;