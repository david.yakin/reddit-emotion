# Reddit Emotion

This is a project to create a site where posts uploaded on the social network Reddit are displayed alongside an analysis of their sentiment. The site combines high functionality with a visually pleasing design. The server is built on NestJS technology, and the client-side is based on React.

## Badges

[![GitLab](https://img.shields.io/badge/GitLab-ffffff?logo=gitlab&logoColor=fc6d26)](https://about.gitlab.com/)
[![Jira](https://img.shields.io/badge/Jira-ffffff?logo=jirasoftware&logoColor=0052CC)](https://www.atlassian.com/software/jira)
[![NodeJS](https://img.shields.io/badge/NodeJS-purple?logo=nodedotjs&logoColor=339933)](https://nodejs.org/en)
[![NPM](https://img.shields.io/badge/NPM-white?logo=npm&logoColor=CB3837)](https://www.npmjs.com/)
[![TypeScript](https://shields.io/badge/TypeScript-3178C6?logo=TypeScript&logoColor=FFF&style=flat-square)](https://www.typescriptlang.org/)
[![NestJs](https://img.shields.io/badge/-NestJs-ea2845?style=flat-square&logo=nestjs&logoColor=white)](https://nestjs.com/)
[![React](https://shields.io/badge/react-black?logo=react&style=for-the-badge)](https://react.dev/)
[![react-hook-form](https://img.shields.io/badge/react_hook_form-081229?logo=reacthookform&logoColor=#EC5990)](https://react-hook-form.com/)
[![Reddit](https://img.shields.io/badge/Reddit-ffffff?logo=reddit&logoColor=ff4500)](https://www.reddit.com/)
[![Hugging Face](https://img.shields.io/badge/%F0%9F%A4%97%20Hugging%20Face-blue)](https://huggingface.co/docs)
[![postgresql](https://img.shields.io/badge/postgresql-4169e1?style=for-the-badge&logo=postgresql&logoColor=white)](https://www.postgresql.org/)

## Tech Stack

**Client:** Node, Typescript, React, MUI, React-Hooks-form

**Server:** Node, Typescript, NestJS, CORS, HuggingFace

**DB:** postgreSQL

## Run Locally

#### Clone the project

```bash
  git clone git@gitlab.com:david.yakin/reddit-emotion.git
```

#### Go to the project directory

```bash
  cd reddit-emotion
```

#### Install server dependencies

```bash
  cd ./server && npm i
```

#### Start the server

```bash
  npm run start:dev
```

#### Install client dependencies

```bash
  cd ../client && npm i
```

#### start the client

```bash
  npm run dev
```

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file for the client:

`VITE_BASE_URL`

To run this project, you will need to add the following environment variables to your .env file for the server:

`WHITE_LIST`

`PORT`

`REDDIT_API`

`REDDIT_CLIENT_ID`

`REDDIT_CLIENT_SECRET`

`REDDIT_USER_AGENT`

`REDDIT_USER_NAME`

`REDDIT_PASSWORD`

`HUGGING_FACE_TOKEN`

`SECRET_KEY`

`SECRET_KEY`

`BASE_URL`

`PG_HOST`

`PG_PORT`

`PG_USER`

`PG_PASSWORD`

`PG_DB`

## postgres connection

to connect to the DB you need to add the following to your .env file and to change the password to your
own password that you gave in the postgres installation and the port is 5432 by default.

## Sending Authentication Token

When making requests to routes protected by the `IsLoginGuard` in your Nest.js application, the client needs to include the authentication token in the request headers.

### How to Send the Token

Include the authentication token in the request headers under the key `x-auth-token`.

## Users API

```http
/users
```

### Register User

#### _request_

```http
POST /
```

#### Request body

| index    | key   | Type   | Description  |
| :------- | :---- | :----- | ------------ |
| email    |       | string | **Required** |
| password |       | string | **Required** |
| name     |       | object | **Required** |
|          | first | string | **Required** |
|          | last  | string | **Required** |

- email must be a valid email address
- Password must be at least nine characters long and contain an uppercase letter, a lowercase letter, a number and one of the following characters !@#$%^&\*-

#### _response_

status code **200**
message **ok**

| index | key   | Type   |
| :---- | :---- | :----- |
| name  |       | object |
|       | first | string |
|       | last  | string |

## User Interface

| index     | key   | Type     |
| :-------- | :---- | :------- |
| \_id      |       | string   |
| email     |       | string   |
| password  |       | string   |
| isAdmin   |       | boolean  |
| createdAt |       | Date     |
| updatedAt |       | Date     |
| history   |       | string[] |
| name      |       | object   |
|           | first | string   |
|           | last  | string   |

### Login User

#### _request_

```http
POST /login
```

#### Request body

| index    | key | Type   | Description  |
| :------- | :-- | :----- | ------------ |
| email    |     | string | **Required** |
| password |     | string | **Required** |

#### _response_

status code: **200**

message: **ok**

data: **Encrypted token**

## Posts API

```http
/posts
```

#### Get all subreddits

```http
  GET /posts/categories
```

- You must send a token in this request in order to receive a response from this endpoint
-

#### Get subreddit posts by subcategory (hot/new/ etc)

```http
  GET /api/posts/:category/:subCategory
```

- You must send a token in this request in order to receive a response from this endpoint

#### Get all specific post details

```http
  GET /api/posts/details/:category/:post_id
```

- You must send a token in this request in order to receive a response from this endpoint

#### Get specific post and his comments

```http
  GET /api/posts/comments/:category/:post_id
```

- You must send a token in this request in order to receive a response from this endpoint

#### Get all history viewed posts by user

```http
  GET /api/users/history
```

- You must send a token in this request in order to receive a response from this endpoint

#### Check if category exist

```http
  GET /api/posts/categories/:category/check
```

- You must send a token in this request in order to receive a response from this endpoint

## Post Interface

```
  {
    _id: string;
    title: string;
    category: string;
    author: string;
    selftext: string;
    sentiment: string;
    created: Date;
    image?: string
  }
```

## Authors

- [@TeddyKT21](https://gitlab.com/TeddyKT21)
- [@david](https://gitlab.com/david.yakin)
