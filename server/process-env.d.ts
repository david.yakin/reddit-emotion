declare global {
  namespace NodeJS {
    interface ProcessEnv {
      PORT: string;
      WHITE_LIST: string;
      REDDIT_API: string;
      REDDIT_CLIENT_ID: string;
      REDDIT_CLIENT_SECRET: string;
      REDDIT_USER_AGENT: string;
      REDDIT_USER_NAME: string;
      REDDIT_PASSWORD: string;
    }
  }
}
export {};
