import { Module, MiddlewareConsumer } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { UsersModule } from './users/users.module';

import { PostsModule } from './posts/posts.module';
import { DataAccessModule } from './data-access/data-access.module';
import { UsersListService } from './users/globalVars/usersList.service';
import { ErrorHandlingModule as ErrorHandlingModule } from './error-handling/handling';
import { SentimentAnalysisModule } from './sentiment-analysis/sentiment-analysis.module';
import HTTPLoggerMiddleware from './logger/HTTPLoggerMiddleware';
import TypeOrmOptions from './data-access/postgreSQL/postgres.config';
import { config } from 'dotenv';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRootAsync(TypeOrmOptions),
    UsersModule,
    PostsModule,
    DataAccessModule,
    SentimentAnalysisModule,
    ErrorHandlingModule,
    ConfigModule.forRoot()
  ],
  exports: [],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(HTTPLoggerMiddleware).forRoutes('*');
  }
}
