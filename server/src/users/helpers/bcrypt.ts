import { compareSync, hashSync } from 'bcrypt';

export const comparePassword = async (
  userPassword: string,
  hashingPassword: string,
) => {
  return compareSync(userPassword, hashingPassword);
};
export const generateHashPassword = (password: string) => {
  return hashSync(password, 10);
};
