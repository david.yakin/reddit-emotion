import { UserRequestDto } from './user.dto';

export class SignUpDto {
  name: UserRequestDto['name'];

  email: UserRequestDto['email'];

  password: UserRequestDto['password'];
}
