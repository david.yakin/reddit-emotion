import { config } from 'dotenv';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from './users.service';
import TypeOrmOptions from 'src/data-access/postgreSQL/postgres.config';
import { UsersModule } from './users.module';
import { UserEntity } from './entity/user.entity';
import { LoginDto } from './dto/login.dto';

describe('UsersService', () => {
  let service: UsersService;

  config(),
    beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        imports: [
          TypeOrmModule.forRootAsync(TypeOrmOptions),
          UsersModule,
          TypeOrmModule.forFeature([UserEntity]),
        ],
        providers: [UsersService],
      }).compile();

      service = module.get<UsersService>(UsersService);
    });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it("login throw error if user doesn't exist", async () => {
    const UserLogin: LoginDto = {
      email: 'example@gmail.com',
      password: '123Sddd#$',
    };

    await expect(service.login(UserLogin)).rejects.toThrow(
      "password or email are incorrect",
    );
  },30000);
});

