import { Injectable, UnauthorizedException } from '@nestjs/common';
import { comparePassword, generateHashPassword } from './helpers/bcrypt';
import { generateToken } from 'src/auth/helpers/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from './entity/user.entity';
import { Repository } from 'typeorm';
import { SignUpDto } from './dto/signUp.dto';
import { LoginDto } from './dto/login.dto';
import { PostEntity } from 'src/posts/entity/post.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    private UserRepository: Repository<UserEntity>,
  ) {}

  async validateUser(userLoginDetails: LoginDto) {
    const { email, password } = userLoginDetails;

    const user = await this.UserRepository.findOne({ where: { email } });
    if (!user)
      throw new UnauthorizedException('password or email are incorrect');

    const isPasswordMatched = await comparePassword(password, user.password);
    if (!isPasswordMatched)
      throw new UnauthorizedException('password or email are incorrect');

    return user;
  }

  async addingNewUser(registrationDetails: SignUpDto) {
    try {
      const newUser = this.UserRepository.create(registrationDetails);
      await this.UserRepository.save(newUser);
      return newUser;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async login(userLoginDetails: LoginDto): Promise<string> {
    try {
      const user = await this.validateUser(userLoginDetails);

      const token = generateToken({
        _id: user.id,
        isAdmin: user.isAdmin,
        name: user.name,
      });

      return token;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async addHistory(id: number, post: PostEntity) {
    try {
      const user = await this.UserRepository.findOne({
        where: { id: id },
        relations: ['history'],
      });
      const history = user.history;
      user.history = [...history, post];
      await this.UserRepository.save(user);
      return user;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getHistory(id: number) {
    try {
      const history = await this.UserRepository.findOne({
        where: { id: id },
        relations: ['history'],
      });
      return history.history;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findAll() {
    return [];
  }

  async signUp(registrationDetails: SignUpDto) {
    try {
      const hashPassword = generateHashPassword(registrationDetails.password);
      registrationDetails = { ...registrationDetails, password: hashPassword };
      const newUser = await this.addingNewUser(registrationDetails);
      return newUser;
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
