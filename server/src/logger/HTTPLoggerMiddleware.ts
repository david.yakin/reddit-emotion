import { NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import logger from './GeneralLogger';

class HTTPLoggerMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    const start = new Date();
    const { method, originalUrl } = req;
    res.on('finish', () => {
      const end = new Date();
      const duration = end.getTime() - start.getTime();
      const statusCode = res.statusCode;
      const statusMessage = res.statusMessage;

      statusCode >= 400
        ? logger(
            'HTTP',
            `${method} ${originalUrl} - ${statusCode} - ${duration}ms ${statusMessage}`,
            'red',
          )
        : logger(
            'HTTP',
            `${method} ${originalUrl} - ${statusCode} - ${duration}ms`,
          );
    });

    next();
  }
}

export default HTTPLoggerMiddleware;
