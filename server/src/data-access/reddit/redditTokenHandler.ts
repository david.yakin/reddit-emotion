import { redditTokenRequest } from "./redditTokenRequestObj";
import { changeRedditToken } from "./redditToken";

export const redditTokenHandler = async () => {
    const redditToken = await redditTokenRequest()
    changeRedditToken(redditToken)

    setTimeout(redditTokenHandler, 12 * 60 * 60 * 1000)
}

















