import axios from 'axios';
import { config } from 'dotenv';
config();

const REDDIT_CLIENT_ID = process.env.REDDIT_CLIENT_ID;
const REDDIT_CLIENT_SECRET = process.env.REDDIT_CLIENT_SECRET;
const REDDIT_USER_AGENT = process.env.REDDIT_USER_AGENT;
const REDDIT_USER_NAME = process.env.REDDIT_USER_NAME;
const REDDIT_PASSWORD = process.env.REDDIT_PASSWORD;

export const redditTokenRequest = async () => {
  const response = await axios.post(
    'https://www.reddit.com/api/v1/access_token',
    {
      grant_type: 'password',
      username: REDDIT_USER_NAME,
      password: REDDIT_PASSWORD,
    },
    {
      auth: {
        username: REDDIT_CLIENT_ID,
        password: REDDIT_CLIENT_SECRET,
      },
      headers: {
        'User-Agent': REDDIT_USER_AGENT,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    },
  );
  return response.data.access_token as string;
};
