import { Module } from '@nestjs/common';
import { PostsController } from './posts.controller';
import { PostsService } from './posts.service';
import { SentimentAnalysisModule } from '../sentiment-analysis/sentiment-analysis.module';
import PostsDal from './post.dal';
import { PostEntity } from './entity/post.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from 'src/users/users.service';
import { UserEntity } from 'src/users/entity/user.entity';

@Module({
  imports: [SentimentAnalysisModule, TypeOrmModule.forFeature([PostEntity, UserEntity])],
  controllers: [PostsController],
  providers: [PostsService, PostsDal, UsersService],
})
export class PostsModule {}
