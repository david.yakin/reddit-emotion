import { HttpStatus, Injectable } from '@nestjs/common';
import PostsDal from './post.dal';
import { SentimentAnalysisService } from 'src/sentiment-analysis/sentiment-analysis.service';
import { CustomHttpException } from 'src/error-handling/custom-error-handl';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PostEntity } from './entity/post.entity';
import { PostDto } from './dto/post.dto';
import { UserEntity } from 'src/users/entity/user.entity';
import { UsersService } from './../users/users.service';

@Injectable()
export class PostsService {
  constructor(
    private postDal: PostsDal,
    private UsersService: UsersService,
    private sentimentService: SentimentAnalysisService,

    @InjectRepository(PostEntity)
    private PostRepository: Repository<PostEntity>,
  ) {}

  async getByIdService(category: string, postId: string, userId: number) {
    try {
      const post: Omit<PostEntity, 'sentiment'> = await this.postDal.getById(
        category,
        postId,
      );
      const sentiment = await this.getPostsSentiment(post);
      const result: PostEntity = { ...post, sentiment };
      if (!sentiment)
        throw new CustomHttpException(`could not get sentiment`, 500);

      await this.addPostToDB(result);
      await this.UsersService.addHistory(userId, result);
      return result;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getPostAndHisComments(category: string, id: string) {
    try {
      const postAndComments = await this.postDal.getPostAndHisComments(
        category,
        id,
      );
      const sentimentPost = await this.getPostsSentiment(postAndComments.post);

      const analyzeComments = await this.getCommentsSentiment(
        postAndComments.comments,
      );
      if (!postAndComments)
        throw new CustomHttpException(
          `Error bringing post and comments by id: ${id}`,
          HttpStatus.FAILED_DEPENDENCY,
        );
      else if (!sentimentPost || !analyzeComments)
        throw new CustomHttpException('could not get sentiment', 500);

      return {
        post: { ...postAndComments.post, sentiment: sentimentPost },
        comments: analyzeComments,
      };
    } catch (error) {
      throw new CustomHttpException(
        `could not find post or comments: ${id}`,
        404,
      );
    }
  }

  private async getCommentsSentiment(comments: { body: string }[]) {
    const analyzeComments = [];
    for (let i = 0; i < comments.length; i++) {
      const element = comments[i];
      const analyzeComment = await this.analyzeCommentsTree(element);
      analyzeComments.push(analyzeComment);
    }
    return analyzeComments;
  }

  async analyzeCommentsTree(comment) {
    const sentiment = await this.sentimentService.getAnalysis(comment.body);
    if (comment.replies) {
      const analyzeReplies = [];
      comment.replies.forEach(async (element) => {
        const ans = await this.analyzeCommentsTree(element);
        analyzeReplies.push(ans);
      });
      return { sentiment: sentiment, ...comment, replies: analyzeReplies };
    } else {
      return { sentiment: sentiment, ...comment };
    }
  }

  async getCategoriesService() {
    try {
      const categories = await this.postDal.getCategories();
      if (!categories) throw new Error("Can't get the categories");
      return categories;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getPostsByCategoryService(category: string, subCategory: string) {
    try {
      const postsByCategory = await this.postDal.getPostsByCategory(
        category,
        subCategory,
      );
      if (!postsByCategory) throw new Error('Post not found!');
      return postsByCategory;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getPostsSentiment(post: { title: string; selftext: string }) {
    try {
      const { title, selftext } = post;
      let sentiment = 'middle';
      if (post.selftext.length < 5)
        sentiment = await this.sentimentService.getAnalysis(title);
      else sentiment = await this.sentimentService.getAnalysis(selftext);

      return sentiment;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async checkCategory(categoryName: string) {
    try {
      await this.postDal.checkCategory(categoryName);
      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async addPostToDB(post: PostDto) {
    try {
      const postDB = this.PostRepository.create(post);
      await this.PostRepository.save(postDB);
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
