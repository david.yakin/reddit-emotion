
export class PostDto {
  _id: string;
  author: string;
  title: string;
  selftext: string;
  sentiment: string;
  created: Date;
  img: string;
  category: string;
}
