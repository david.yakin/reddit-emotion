interface RedditComment {
  kind: string;
  data: {
    body: string;
    created: number;
    author: string;
    replies: RedditComment[];
  };
}

export default RedditComment;
