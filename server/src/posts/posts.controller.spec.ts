import { Test, TestingModule } from '@nestjs/testing';
import { PostsController } from './posts.controller';
import { CustomHttpException } from '../error-handling/custom-error-handl';
import { CategoryDataObj } from 'src/types';
import { PostsService } from './posts.service';
import { SentimentAnalysisService } from '../sentiment-analysis/sentiment-analysis.service';
import e from 'cors';
import PostsDal from './post.dal';
import { PostsModule } from './posts.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostEntity } from './entity/post.entity';
import TypeOrmOptions from 'src/data-access/postgreSQL/postgres.config';
import { UsersService } from 'src/users/users.service';
import { UserEntity } from 'src/users/entity/user.entity';

const mockPosts = [
  {
    id: 'my-id',
    title: 'title',
    img: 'https://cdn.pixabay.com/photo/2023/07/24/09/14/coccinellidae-8146623_1280.jpg',
    category: 'category',
    selftext: 'category',
    author: 'david yakin',
    created: new Date('2024-03-17'),
  },
];

describe('PostsController', () => {
  let controller: PostsController;
  let service: PostsService;
  let dalService: PostsDal;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRootAsync(TypeOrmOptions),
        TypeOrmModule.forFeature([PostEntity, UserEntity]),
      ],
      controllers: [PostsController],
      providers: [PostsService, PostsDal,UsersService,SentimentAnalysisService],
    }).compile();

    controller = module.get<PostsController>(PostsController);
    service = module.get<PostsService>(PostsService);
    dalService = module.get<PostsDal>(PostsDal);
  });

  it('should return posts by category not found', async () => {
    jest
      .spyOn(dalService, 'getPostsByCategory')
      .mockImplementation(function () {
        throw new Error('Not failed to fetch post by category from the API');
      });
    await expect(async () =>
      controller.getPostsByCategory('a', 'b'),
    ).rejects.toThrow(Error);
  });

  it(`should return categories`, async () => {
    const mockCategoriesData: CategoryDataObj[] = [
      {
        name: `name`,
        title: `title`,
        display_name: `display_name`,
      },
    ];
    jest
      .spyOn(dalService, `getCategories`)
      .mockImplementation(async () => mockCategoriesData);
    expect(await controller.getCategories()).toBe(mockCategoriesData);
  },30000);
});
