import { Test, TestingModule } from '@nestjs/testing';
import { PostsService } from './posts.service';
import PostsDal from './post.dal';
import { SentimentAnalysisService } from 'src/sentiment-analysis/sentiment-analysis.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostEntity } from './entity/post.entity';
import { PostsModule } from './posts.module';
import TypeOrmOptions from 'src/data-access/postgreSQL/postgres.config';
import { UserEntity } from 'src/users/entity/user.entity';
import { UsersService } from 'src/users/users.service';

describe('PostsService', () => {
  let service: PostsService;
  let postDal: PostsDal;
  let sentimental: SentimentAnalysisService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      // imports: [
      //   TypeOrmModule.forRootAsync(TypeOrmOptions),
      //   TypeOrmModule.forFeature([PostEntity, UserEntity]),
      // ],
      providers: [SentimentAnalysisService],
    }).compile();

    // postDal = module.get<PostsDal>(PostsDal);
    // service = module.get<PostsService>(PostsService);
    sentimental = module.get<SentimentAnalysisService>(
      SentimentAnalysisService,
    );
  });

  it('should be defined', () => {
    expect(sentimental).toBeDefined();
  });

  // it('test sentiment analysis of post without text', async () => {
  //   const mockPost = {
  //     selftext: '',
  //     title:
  //       'The Splendor of Ordinary Life: Unveiling the Beauty in Everyday Moments',
  //   };
  //   const result = await service.getPostsSentiment(mockPost);
  //   expect(result).toEqual('POSITIVE');
  // });
});
