import { Column, Entity, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class PostEntity {
  @PrimaryColumn({ type: 'varchar' })
  _id: string;

  @Column({ type: 'varchar' })
  author: string;

  @Column({ type: 'varchar' })
  title: string;

  @Column({ type: 'varchar' })
  selftext: string;

  @Column({ type: 'varchar' })
  sentiment: string;

  @Column({ type: 'date' })
  created: Date;

  @Column({ type: 'varchar', nullable: true })
  img: string;

  @Column({ type: 'varchar' })
  category: string;
}
