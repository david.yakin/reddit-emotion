import { LoggerService, Injectable } from '@nestjs/common';
import * as winston from 'winston';
import { blue, yellow, red, gray, magenta } from "colorette";

@Injectable()
export class CustomLoggerService implements LoggerService {
  private logger: winston.Logger;

  constructor() {
    this.logger = winston.createLogger({
      level: 'info',
      transports: [
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.colorize({ all: true }),
            winston.format.simple()
          )
        }),
        // new winston.transports.Console({ level: 'info' }),
        new winston.transports.File({ filename: 'application.log', level: 'info' }),
        new winston.transports.File({ filename: 'filelog-error.log', level: 'error' })
      ]
    });
  }

  log(message: string) {
    this.logger.info(magenta(message));
  }

  error(message: string, trace: string) {
    this.logger.error(red(message), { trace });
  }

  warn(message: string) {
    this.logger.warn(yellow(message));
  }

  debug(message: string) {
    this.logger.debug(blue(message));
  }

  verbose(message: string) {
    this.logger.verbose(gray(message));
  }
}
