import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
// import { CustomLoggerService } from './customLogger.service';

// @Injectable()
// export class LoggerMiddleware implements NestMiddleware {
//   constructor(private readonly logger: CustomLoggerService) {}

//   use(req: Request, res: Response, next: NextFunction): void {
//     const { method, originalUrl } = req;
//     const message = `${method} ${originalUrl}`;

//     this.logger.log(message);

//     res.on('finish', () => {
//       const { statusCode } = res;
//       const responseLog = `${method} ${originalUrl} ${statusCode}`;
//       this.logger.log(responseLog);
//     });

//     next();
//   }
// }
