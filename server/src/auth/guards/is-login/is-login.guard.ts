import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { verifyToken } from '../../helpers/jwt';
import {
  TokenInterface,
  UserPayloadInterface,
} from 'src/auth/interfaces/TokenInterface';

@Injectable()
export class IsLoginGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    try {
      const request: UserPayloadInterface = context.switchToHttp().getRequest();
      const { headers } = request;

      const Token = headers['x-auth-token'];
      if (!Token) return false;

      const UserInfo = verifyToken(Token) as TokenInterface;

      if (!UserInfo) return false;

      request.user = UserInfo;
      return true;
    } catch (error) {
      return false;
    }
  }
}
