import { Test, TestingModule } from '@nestjs/testing';
import { AdminGuard } from './is-admin.guard';

describe('Is admin guard', () => {
  let guard: AdminGuard;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AdminGuard],
    }).compile();

    guard = module.get<AdminGuard>(AdminGuard);
  });

  it('should be defined', () => {
    expect(guard).toBeDefined();
  });

  it('should return false if no token is provided', async () => {
    expect(await guard.canActivate({} as any)).toBeFalsy();
  });

  it('should return false if invalid token is provided', async () => {
    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            'x-auth-token': 'invalid',
          },
        }),
      }),
    } as any;
    expect(await guard.canActivate(context)).toBeFalsy();
  });

  it('should return true if valid token is provided and isadmin true ', async () => {
    // Mock valid token
    jest
      .spyOn(require('../../helpers/jwt'), 'verifyToken')
      .mockReturnValueOnce({
        id: 1,
        username: 'test',
        isAdmin: true,
      });

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            'x-auth-token': 'valid',
          },
        }),
      }),
    } as any;
    expect(await guard.canActivate(context)).toBeTruthy();
  });
});
