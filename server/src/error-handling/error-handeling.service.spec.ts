import { Test, TestingModule } from '@nestjs/testing';
import { HttpExceptionFilterError } from './error-handeling.service';
import { CustomLoggerService } from 'src/custom-logger/customLogger.service';

describe('ErrorHandelingService', () => {
  let service: HttpExceptionFilterError;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HttpExceptionFilterError, CustomLoggerService],
    }).compile();

    service = module.get<HttpExceptionFilterError>(HttpExceptionFilterError);
  });

  it('exists', () => {
    expect(service).toBeDefined();
  });

});
