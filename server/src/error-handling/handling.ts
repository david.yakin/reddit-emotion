import { Global, Module } from '@nestjs/common';
import { HttpExceptionFilterError } from './error-handeling.service';
import { CustomLoggerService } from '../custom-logger/customLogger.service';

@Global()
@Module({
  providers: [HttpExceptionFilterError,CustomLoggerService],
  exports: [HttpExceptionFilterError]
})
export class ErrorHandlingModule {}
