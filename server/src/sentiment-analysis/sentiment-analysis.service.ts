import { Injectable } from '@nestjs/common';
import { HfInference, TextClassificationOutput } from '@huggingface/inference';
import { config } from 'dotenv';
config();
const HUGGING_FACE_TOKEN = process.env.HUGGING_FACE_TOKEN || '';

@Injectable()
export class SentimentAnalysisService {
  private static CERTAINTY_DIFFERENTIAL = 0.2;
  private static CHUNK_SIZE = 512;
  private huggingFace: HfInference;

  constructor() {
    this.huggingFace = new HfInference(HUGGING_FACE_TOKEN);
  }

  async getAnalysis(text: string) {
    const chunkFideText = this.breakToChunks(text);
    const sentimentOutput = await this.getRawScore(chunkFideText);
    return this.calculateByScore(sentimentOutput);
  }

  private breakToChunks(text: string) {
    if (typeof text !== 'string') return [];
    const chunckSize = SentimentAnalysisService.CHUNK_SIZE;
    const numOfChuncks = Math.ceil(text.length / chunckSize);
    const chunkFiedText = [];
    for (let i = 0; i < numOfChuncks; i++) {
      chunkFiedText[i] = text.substring(
        i * chunckSize,
        Math.min((i + 1) * chunckSize, text.length),
      );
    }
    return chunkFiedText;
  }

  private async getRawScore(chunkFiedText: string[]) {
    const getEvaluations = chunkFiedText.map(
      async (textChunk) =>
        await this.huggingFace.textClassification({
          model: 'distilbert-base-uncased-finetuned-sst-2-english',
          inputs: textChunk,
        }),
    );
    return await Promise.all(getEvaluations);
  }
  private calculateByScore(sentiments: TextClassificationOutput[]) {
    const sentimentSum: TextClassificationOutput =
      this.aggregateSentiments(sentiments);
    if (this.ValuesTooClose(sentimentSum, sentiments.length)) return 'NEUTRAL';
    return this.getBiggerSentiment(sentimentSum);
  }
  private aggregateSentiments(sentiments: TextClassificationOutput[]) {
    const sentimentSum: TextClassificationOutput = [
      { label: 'POSITIVE', score: 0 },
      { label: 'NEGATIVE', score: 0 },
    ];
    sentiments.forEach((sentimet) => {
      if (sentimet[0].label === 'POSITIVE') {
        sentimentSum[0].score += sentimet[0].score;
        sentimentSum[1].score += sentimet[1].score;
      } else {
        sentimentSum[1].score += sentimet[0].score;
        sentimentSum[0].score += sentimet[1].score;
      }
    });
    return sentimentSum;
  }

  private ValuesTooClose(
    sentiment: TextClassificationOutput,
    numOfChunks: number,
  ) {
    return (
      Math.abs(sentiment[0].score - sentiment[1].score) <
      SentimentAnalysisService.CERTAINTY_DIFFERENTIAL * Math.sqrt(numOfChunks)
    );
  }
  getBiggerSentiment(sentiment: TextClassificationOutput) {
    const labelOne = sentiment[0].label;
    const labelTwo = sentiment[1].label;
    const scoreOne = sentiment[0].score;
    const scoreTwo = sentiment[1].score;
    return scoreTwo > scoreOne ? labelTwo : labelOne;
  }
}
